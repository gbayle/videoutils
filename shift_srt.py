#!/bin/env python
# -*- coding: utf-8 -*-

"""
Décale les indications de temps d’un fichier <srt>

Usage:
    shift_srt [ -d ] <f> [<t> ...]

Arguments:
    <f>  Fichier .srt à analyser
    <t> ... Liste de couples a0-b0 a1-b1 de valeurs en secondes

Options:
    -h, --help  affiche ce message; et sort.
    -d          dry-run, calcule juste facteur et décalage

Exemple:

    shift_srt soustitre.srt 20-50           # Retarder de 30 secondes
    shift_srt soustitre.srt 20-50 3800-3875 # Retarder de 30 secondes, et dilater le temps
    shift_srt soustitre.srt 20-50 3800-3875 7200-7300 # Idem, et changer la dilation après 3800s
"""

from datetime import datetime, timedelta
import docopt

def make_td(a_time):
    """
    Renvoie un objet timedelta à partir des chaînes en argument

    a_time : liste de chaînes [heure, minute, seconde, milliseconde]
    """
    s_h, s_m, s_s, s_ms = a_time
    seconds = int(s_h) * 3600 + int(s_m) * 60 + int(s_s)
    microseconds = int(s_ms) * 1000
    return timedelta(seconds=seconds, microseconds=microseconds)

def format_time_mark(td_x):
    """
    Formate td_x en hh:mm:ss,SSS
    """
    # Pour préparer le formatage, on indique un jour arbitraire dans un datetime
    dt_0 = datetime(year=2000, month=1, day=1)
    dt_x = dt_0 + td_x
    # Maintenant on a tous les éléments pour le format hh:mm:ss,SSS
    fmt = "{0:02}:{1:02}:{2:02},{3:03}"
    s_x = fmt.format(dt_x.hour, dt_x.minute, dt_x.second, dt_x.microsecond // 1000)
    return s_x

def get_affine_coefs(t, i, list_x_2_y):
    """
    Renvoie td_shift, shift_factor, i où
    - td_shift est le décalage à appliquer
    - shift_factor est la dilatation à appliquer
    - i est l’index de list_x_2_y qui sert de base à la conversion

    Arguments:
    - t : le temps à «encadrer» si possible
    - i : le dernier i utilisé comme base; impératif: i+1 < len(list_x_2_y)
    - list_x_2_y : liste de couples de temps (avant, apres), en secondes
    """
    nb_couples = len(list_x_2_y)
    if nb_couples == 0:
        # Aucun point: on ne change rien
        shift_value = 0
        shift_factor, j = 1, 0
    elif nb_couples == 1:
        # Un seul point: décalage simple
        a0, b0 = list_x_2_y[0]
        shift_value = b0 - a0
        shift_factor, j = 1, 0
    else:
        # Chercher un encadrement de t
        j = i
        while j < nb_couples - 1:
            a1, b1 = list_x_2_y[j + 1]
            if a1 > t:
                break
            j += 1
        # Si pas trouvé, on prend les deux derniers points
        if j >= nb_couples - 1:
            j = nb_couples - 2
        # Extraction des points
        a0, b0 = list_x_2_y[j]
        a1, b1 = list_x_2_y[j+1]
        # Calcul de la pente et du décalage
        shift_factor = (b1 - b0) / (a1 - a0)
        shift_value = b0 - (a0 * shift_factor)
    # Renvoyer le résultat trouvé
    td_shift = timedelta(seconds=shift_value)
    return td_shift, shift_factor, j

def load_srt(f_name, list_x_2_y):
    """
    Affiche chaque ligne de f_name, en modifiant les indications de temps
    selon la fonction affine: b = shift_value + (a * shift_factor)

    - f_name : le fichier .srt dont il faut décaler les marqueurs
    - shift_value : décalage en secondes par rapport à l’instant 0 du sous-titre original
    - shift_factor : coefficient multiplicatif pour la vitesse
    """

    i_list = 0
    if not f_name is None:
        fd_in = open(f_name, "r")
        for line in fd_in:
            # Reconnaître les lignes au format 00:22:24,140 --> 00:22:28,736
            if len(line) > 20 and line[13:16] == "-->":
                # Découper la ligne selon ses champs numériques
                fields = line.replace(" --> ", ":").replace(",", ":").split(":")
                td_a = make_td(fields[0:4]) # Champs 0-3 : timedelta
                td_b = make_td(fields[4:8]) # Champs 4-7 : timedelta

                # Formule b = shift_value + (a * shift_factor)
                t_seconds = td_a.total_seconds()
                td_shift, shift_factor, i_list = get_affine_coefs(t_seconds, i_list, list_x_2_y)

                # Décaler les dates et les formater : hh:mm:ss,SSS
                s_a = format_time_mark(td_shift + (td_a * shift_factor))
                s_b = format_time_mark(td_shift + (td_b * shift_factor))
                print("{0} --> {1}".format(s_a, s_b))
            else:
                # Afficher les autres lignes inchangées
                print(line, end='')

def do_main():
    """
    Interprétation des arguments avant de faire le travail.
    """
    dict_options = docopt.docopt(__doc__, version="0.0")
    f_name = dict_options["<f>"]
    tstamps = dict_options["<t>"]

    # Lister les valeurs-repères de temps à convertir, en secondes
    list_x_2_y = []
    for tt in tstamps:
        t0, t1 = tt.split("-")
        list_x_2_y.append((float(t0), float(t1)))

    if dict_options["-d"]:
        print("Valeurs de temps à convertir, en secondes:")
        print(list_x_2_y)
    else:
        load_srt(f_name, list_x_2_y)

if __name__ == "__main__":
    do_main()
