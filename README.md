Ce script élémentaire (en Python3) décale les indications de temps d’un fichier <srt>

Usage:

    shift_srt [ -d ] <f> [<t> ...]

Arguments:

    <f>  Fichier .srt à analyser
    <t> ... Liste de couples a0-b0 a1-b1 de valeurs en secondes

Options:

    -h, --help  affiche ce message; et sort.
    -d          dry-run, calcule juste facteur et décalage

Exemples:

    shift_srt soustitre.srt 20-50           # Retarder de 30 secondes
    shift_srt soustitre.srt 20-50 3800-3875 # Retarder de 30 secondes, et dilater le temps
    shift_srt soustitre.srt 20-50 3800-3875 7200-7300 # Idem, et changer la dilation après 3800s
